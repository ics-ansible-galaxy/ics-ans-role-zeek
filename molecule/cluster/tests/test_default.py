import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('zeek-mgr-default')


def test_zeek_is_installed(host):
    zeek = host.service("zeek.service")
    assert zeek.is_enabled
