{% if zeek_packages is defined and zeek_packages|length %}
{% for package in zeek_packages %}
/usr/bin/scl enable devtoolset-7 "{{ zkg_bin }} install --force {{ package }}"
{% endfor %}
{% endif %}
