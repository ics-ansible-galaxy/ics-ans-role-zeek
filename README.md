# ics-ans-role-zeek

This Ansible role automates the process of deploying and configuring Zeek, an open-source network security monitor. 

Ansible role to install zeek.

### standalone mode
```mermaid
flowchart LR
    A[clients] --- B((Network router))
    B --- C[servers]
    B -."mirror".-D[zeek] 
```

### cluster mode
```mermaid
flowchart LR
    A[clients] --- B(edge switch)
    B---C(Tap)
    C---D((core router))
    D---E(Tap)
    E---F(edge switch)
    F---G[servers]
    C-.-H[network packet broker]
    E-.-H
    D---J(edge switch)
    J---K(tap)
    K---L{{firewall}}
    L---M(Tap)
    M---N{{Internet}}
    K-.-H
    M-.-H
    H---a(zeek)
    H---b(zeek)
    H---c(zeek)
```

## Role Variables

```yaml
# `standalone` node or `cluster` configuration variables
zeek_cluster_type: standalone
zeek_standalone_interface: eth0

#for each zeek worker node define the following or set this as a global value where pertinent
zeek_worker_interface: eth0

# change to true on group or host variable
zeek_wrk: false
zeek_mgr: false

cluster_settings:
  - host: localhost
    role: logger
    lb_procs: 0
    cpu: 0
  - host: localhost
    role: manager
    lb_procs: 0
    cpu: 1
  - host: localhost
    role: proxy
    lb_procs: 0
    cpu: 1
  - host: localhost
    role: worker
    lb_procs: 2
    interface: eth0
    cpu: 0,1

zeek_networks:
  - name: "office network"
    subnet: "10.3.0.0/22"
  - name: "dmz network"
    subnet: "10.4.0.0/22"
zkg_bin: /opt/zeek/bin/zkg
zkg_install_script: /opt/zeek/bin/devtools.sh

# zeek filters
zeek_restrict_ip:
  - 1.1.1.1
  - 1.1.1.2
  - 1.1.1.3
  - 1.1.1.4
zeek_restrict_port:
  - 8009
  - 7680

# general zeek settings
zeek_log_rotate: 3600
zeek_expire_interval: "4day"
zeek_random_salt: "48hvrnvjbnvuehvnvlk482yhnvjfb;vjkvbjsdvktunvk"
zeek_distribution_list: user@ess.eu,user1@ess.eu

zeek_packages:
  - https://github.com/mitre-attack/bzar
  - zeek-af_packet-plugin
  - zeek/hosom/file-extraction
  - zeek/j-gras/add-interfaces
  - zeek/j-gras/zeek-af_packet-plugin
  - zeek/jsiwek/zeek-cryptomining
  - zeek/mitrecnd/bro-http2
  - zeek/ncsa/bro-is-darknet
  - zeek/salesforce/hassh
  - zeek/salesforce/ja3
  - zeek/sethhall/domain-tld
zkg_bin: /opt/zeek/bin/zkg
zkg_install_script: /opt/zeek/bin/devtools.sh
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-zeek
```

## License

BSD 2-clause
---
